package com.example.gestureapp;

import androidx.core.view.GestureDetectorCompat;

import android.app.Activity;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends Activity implements
        GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener{
private TextToSpeech textToSpeech;
    private static final String DEBUG_TAG = "Gestures";
    private GestureDetectorCompat mDetector;
    private TextView textView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView=findViewById(R.id.textView);
        mDetector = new GestureDetectorCompat(this,this);
textToSpeech=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
    @Override
    public void onInit(int status) {
        if(status==TextToSpeech.SUCCESS){
            int lang=textToSpeech.setLanguage(Locale.US);
            if (lang==TextToSpeech.LANG_MISSING_DATA||lang==TextToSpeech.LANG_NOT_SUPPORTED){
                Log.e("TTS","language is not supported");
            }else{
                Log.e("TTS","Lang supported");
            }
            System.out.println("success");
        }else
        {
            Toast.makeText(getApplicationContext(),"TTS Initialization failed",Toast.LENGTH_SHORT).show();
        }
    }
});
        mDetector.setOnDoubleTapListener(this);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        if (this.mDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent event) {
        Log.d(DEBUG_TAG,"onDown: " + event.toString());
        textView.setText("Down");
        makeSpeech(textView);


        return true;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2,
                           float velocityX, float velocityY) {
        Log.d(DEBUG_TAG, "onFling: " + event1.toString() + event2.toString());
        textView.setText("Fling");
        return true;
    }

    @Override
    public void onLongPress(MotionEvent event) {
        Log.d(DEBUG_TAG, "onLongPress: " + event.toString());
        textView.setText("Long press");
        makeSpeech(textView);
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX,
                            float distanceY) {
        Log.d(DEBUG_TAG, "onScroll: " + event1.toString() + event2.toString());
        textView.setText("Scroll");
        makeSpeech(textView);

        return true;
    }

    @Override
    public void onShowPress(MotionEvent event) {
//        Log.d(DEBUG_TAG, "onShowPress: " + event.toString());
//        textView.setText("onShowPress");
//        makeSpeech(textView);

    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
//        Log.d(DEBUG_TAG, "onSingleTapUp: " + event.toString());
//        textView.setText("onSingle Tap Up");
//        makeSpeech(textView);

        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent event) {
//        Log.d(DEBUG_TAG, "onDoubleTap: " + event.toString());
//        textView.setText("Double Tap");
//        makeSpeech(textView);

        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDoubleTapEvent: " + event.toString());
        textView.setText(" DoubleTap Event");
        makeSpeech(textView);

        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {
        Log.d(DEBUG_TAG, "onSingleTapConfirmed: " + event.toString());
        textView.setText("Single Tap Confirmed");
        makeSpeech(textView);

        return true;
    }
    private void makeSpeech(TextView textView){
        String data=textView.getText().toString();
        textToSpeech.speak(data,TextToSpeech.QUEUE_FLUSH,null);
    }
}
